import React, {Component} from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import SearchVenuePage from "./searchResultPage/searchVenuePage/SearchVenuePage.jsx";
import VenuePage from "./venuePage/VenuePage.jsx";
import SearchPage from "./searchPage/SearchPage.jsx";

import "./app.scss";

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact={true} path="/" component={SearchPage}/>
          <Route path="/search/:searchText" component={SearchVenuePage}/>
          <Route path="/venue/:venueId" component={VenuePage}/>
          <Route render={function () {
            return <p>{"404 not found"}</p>;
          }}/>
        </Switch>
      </Router>
    );
  }
}

export default App;
