import React, {Component} from "react";

import "./footer.scss";

class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div>{"About Us"}</div>
        <div>{"Contact"}</div>
        <div>{"Blog"}</div>
      </div>
    );
  }
}

export default Footer;
