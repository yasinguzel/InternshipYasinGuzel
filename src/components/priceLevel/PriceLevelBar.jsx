import React, {Component} from "react";

import "./priceLevelBar.scss";

import priceLevel1 from "../../../assets/img/price-level/level1/rectangle-10.png";
import priceLevel2 from "../../../assets/img/price-level/level2/rectangle-10.png";
import priceLevel3 from "../../../assets/img/price-level/level3/rectangle-10.png";
import priceLevel4 from "../../../assets/img/price-level/level4/rectangle-10.png";
import priceIcon from "../../../assets/img/price-icon.png";
import backgroundImage from "../../../assets/img/price-level-bar-background.png";

const levels = [priceLevel1, priceLevel2, priceLevel3, priceLevel4];

class PriceLevelBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      levelBar: []
    };
  }

  componentWillMount() {
    for (let i = 0; i < this.props.tier; i++) {
      this.state.levelBar.push(levels[i]);
    }
  }

  render() {
    const backgroundImageStyle = {
      backgroundImage: `url(/${backgroundImage})`
    };

    return (
      <div className="price-level-bar-container">
        <img src={`/${priceIcon}`} className="price-level-bar-price-icon"/>
        <div className="price-level-bar-tier-container" style={backgroundImageStyle}>
          {this.state.levelBar.map(level => (<img key={level} src={`/${level}`} alt=""/>))}
        </div>
      </div>
    );
  }
}

export default PriceLevelBar;
