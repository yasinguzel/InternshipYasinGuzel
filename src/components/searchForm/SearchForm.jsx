import React, {Component} from "react";
import PropTypes from "prop-types";

import "./search-form.scss";

class SearchForm extends Component {
  static propTypes = {
    onQueryChange: PropTypes.func.isRequired,
    onLocationChange: PropTypes.func.isRequired,
    query: PropTypes.string.isRequired,
    locationText: PropTypes.string.isRequired,
    onHandleSubmit: PropTypes.func.isRequired
  };

  handleQueryInputChange = (event) => {
    this.props.onQueryChange(event.target.value);
  };

  handleLocationTextInputChange = (event) => {
    this.props.onLocationChange(event.target.value);
  };

  handleSubmit = (event) => {
    if ((this.props.query !== "") && (this.props.locationText !== "")) {
      this.props.onHandleSubmit();
    }
    event.preventDefault();
  };

  render() {
    const {query, locationText} = this.props;

    return (
      <form className="search-form" onSubmit={this.handleSubmit}>
        <input type="text" placeholder="I'm looking for" value={query}
               onChange={this.handleQueryInputChange}
               className="search-form-query-input search-form-input-text search-form-input"/>
        <input type="text" placeholder="Istanbul" value={locationText}
               onChange={this.handleLocationTextInputChange}
               className="search-form-location-text-input search-form-input-text search-form-input"/>
        <input type="submit" className="search-form-submit search-form-input" value=""/>
      </form>
    );
  }
}

export default SearchForm;
