import React, {Component} from "react";

import Footer from "../components/footer/Footer.jsx";
import SearchForm from "../components/searchForm/SearchForm.jsx";

import "./search-page.scss";

import backgroundImage from "../../assets/img/background.png";
import headerLogo from "../../assets/img/search-page-header-logo.png";

class SearchPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locationText: "",
      query: ""
    };
  }

  handleQueryInput = (query) => {
    this.setState({
      query
    });
  };

  handleLocationTextInput = (locationText) => {
    this.setState({
      locationText
    });
  };

  handleButtonPress = () => {
    this.props.history.push(`/search/${this.state.query} ${this.state.locationText}`);
  };

  render() {
    const backgroundImageStyle = {
      backgroundImage: `url(${backgroundImage})`
    };

    return (
      <div>
        <div className="search-page-container" style={backgroundImageStyle}>
          <img src={headerLogo} className="search-page-header-logo"/>
          <div className="search-page-header-text">{"Lorem ipsum dolor sit!"}</div>
          <div className="search-page-content-text">
            {"Lorem ipsum dolor sit amet, consectetur adipiscing elit,"}
            <br/>
            {"sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."}
          </div>
          <div className="search-page-search-form">
            <SearchForm query={this.state.query}
                        locationText={this.state.locationText}
                        onHandleSubmit={this.handleButtonPress}
                        onQueryChange={this.handleQueryInput}
                        onLocationChange={this.handleLocationTextInput}/>
          </div>
        </div>
        <div className="search-page-footer">
          <Footer/>
        </div>
      </div>
    );

  }
}

export default SearchPage;
