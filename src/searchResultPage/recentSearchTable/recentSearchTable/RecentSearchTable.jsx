import React, {Component} from "react";

import RecentSearchTableItem from "../recentSearchTableItem/RecentSearchTableItem.jsx";

import "./recent-search-table.scss";

import line from "../../../../assets/img/line@3x.png";

class RecentSearchTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recentSearchData: []
    };
  }

  componentWillReceiveProps() {
    let recentSearchData = "";

    if (localStorage.getItem("recentSearchData") === null) {
      recentSearchData = "";
    } else {
      recentSearchData = JSON.parse(localStorage.getItem("recentSearchData"));
    }
    this.setState({
      recentSearchData
    });
  }

  handleClickRecentSearchTableItem = (query, locationText) => {
    this.props.onHandleRecentSearchDataClick(query, locationText);
  };

  render() {
    const {recentSearchData} = this.state;

    return (
      <div className="recent-search-list-wrapper">
        <div className="recent-search-list-header">{"RECENT SEARCHES"}</div>
        <img src={`/${line}`} className="recent-search-list-header-line"/>
        <div className="recent-search-list-container">
          {recentSearchData ?
            recentSearchData.map(data => (
              <RecentSearchTableItem key={data.query + data.locationText}
                                    query={data.query}
                                    locationText={data.locationText}
                                    onHandleClickRecentSearchTableItem={this.handleClickRecentSearchTableItem}/>
            )) :
            ""}
        </div>
      </div>
    );
  }
}

export default RecentSearchTable;
