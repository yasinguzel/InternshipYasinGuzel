import React, {Component} from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";

import "./recent-search-table-item.scss";

class RecentSearchTableItem extends Component {
  static propTypes = {
    onHandleClickRecentSearchTableItem: PropTypes.func.isRequired,
    query: PropTypes.string.isRequired,
    locationText: PropTypes.string.isRequired
  };

  handleClickRecentSearchTableItem = () => {
    const {query, locationText} = this.props;

    this.props.onHandleClickRecentSearchTableItem(query, locationText);
  };

  render() {
    const {query, locationText} = this.props;

    return (
      <div className="recent-search-list-item-container">
        <Link to={`/search/${query} ${locationText}`}
              className="recent-search-list-item-link">
          <div onClick={this.handleClickRecentSearchTableItem}>{`${query} in ${locationText}`}</div>
        </Link>
      </div>
    );
  }
}

export default RecentSearchTableItem;
