import React, {Component} from "react";
import "whatwg-fetch";

import SearchForm from "../../components/searchForm/SearchForm.jsx";
import VenueList from "../venueList/VenueList.jsx";
import RecentSearchTable from "../recentSearchTable/recentSearchTable/recentSearchTable.jsx";
import Footer from "../../components/footer/Footer.jsx";

import accessKeys from "../../connection/accessKeys.js";

import "./searchVenuePage.scss";

import searchBarLogo from "../../../assets/img/search-bar-logo.png";

const RECENT_SEARCH_LIST_LIMIT = 10;

class SearchVenuePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: "",
      locationText: "",
      venuesData: []
    };
  }

  componentWillMount() {
    const handleSearchText = this.props.location.pathname;
    const searchTextArray = handleSearchText.toLowerCase().replace(/[^a-z0-9-\s]+/, "")
      .split(" ");

    this.setState({
      query: searchTextArray[0].split("search/")[1],
      locationText: searchTextArray[1]
    }, this.handleSubmit);
  }

  handleQueryInput = (query) => {
    this.setState({
      query
    });
  };

  handleLocationTextInput = (locationText) => {
    this.setState({
      locationText
    });
  };

  handleRecentSearchDataClick = (query, locationText) => {
    this.setState({
      query,
      locationText
    });
    this.fetchData({
      query,
      locationText
    });
  };

  handleSubmit = () => {

    const {query, locationText} = this.state;

    this.props.history.push(`/search/${query} ${locationText}`);

    this.fetchData({
      query,
      locationText
    });
    this.addRecentSearchDataToLocalStorage({
      query,
      locationText
    });
  };

  addRecentSearchDataToLocalStorage = (data) => {
    const dataArray = localStorage.getItem("recentSearchData") ?
      JSON.parse(localStorage.getItem("recentSearchData")) :
      [];

    if (dataArray.length >= RECENT_SEARCH_LIST_LIMIT) {
      dataArray.splice(-1, 1);
    }

    const isItemInList = (list, item) => !!list.find(
      listItem => ((listItem.query === item.query) && (listItem.locationText === item.locationText))
    );

    if (!isItemInList(dataArray, data)) {
      dataArray.unshift(data);
    }
    localStorage.setItem("recentSearchData", JSON.stringify(dataArray));
  };


  fetchData = (venuesQuery) => {
    this.setState({
      venuesData: []
    });

    const {clientId, clientSecret} = accessKeys;
    const {locationText, query} = venuesQuery;

    const URL = `https://api.foursquare.com/v2/venues/explore?near=${locationText
      }&query=${query
      }&client_id=${clientId
      }&client_secret=${clientSecret
      }&venuePhotos=1&v=20120609`;

    fetch(URL).then(res => res.json())
      .then((json) => {
        this.setState({
          venuesData: json.response.groups[0].items
        });
      });
  };

  render() {
    const {query, locationText, venuesData} = this.state;

    return (
      <div className="searchable-venues-table-container">
        <div className="searchable-venues-table-search-form">
          <img src={`/${searchBarLogo}`}
               className="searchable-venues-table-search-form-logo"/>
          <SearchForm query={query}
                      locationText={locationText}
                      onQueryChange={this.handleQueryInput}
                      onLocationChange={this.handleLocationTextInput}
                      onHandleSubmit={this.handleSubmit}/>
        </div>
        <div className="searchable-venues-table-content-container">
          <VenueList venues={venuesData}
                     onHandleVenueId={this.props.handleVenueId}/>
          <RecentSearchTable onHandleRecentSearchDataClick={this.handleRecentSearchDataClick}/>
        </div>
        <div className="searchable-venues-table-footer">
          <Footer/>
        </div>
      </div>
    );
  }
}

export default SearchVenuePage;
