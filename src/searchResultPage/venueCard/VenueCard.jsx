import React, {Component} from "react";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";

import PriceLevelBar from "../../components/priceLevel/PriceLevelBar.jsx";

import "./venue-card.scss";

import hereNowCountIcon from "../../../assets/img/here-now-icon.png";
import filterImage from "../../../assets/img/filter-image.png";
import triangle from "../../../assets/img/triangle.png";
import line from "../../../assets/img/line.png";

class VenueCard extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    hereNowCount: PropTypes.number,
    priceInfo: PropTypes.object,
    rating: PropTypes.number,
    photo: PropTypes.string
  };

  static defaultProps = {
    name: "Name not found",
    hereNowCount: 0,
    rating: 0
  };

  render() {
    const {id, name, hereNowCount, priceInfo, rating, photo} = this.props;
    const tier = priceInfo ?
      priceInfo.tier :
      0;
    const venueImage = {
      backgroundImage: `url(${photo})`
    };

    const filterImageSytle = {
      backgroundImage: `url(/${filterImage})`
    };

    return (
      <div style={venueImage} className="venue-card-wrapper">
        <Link to={`/venue/${id}`} style={{
          textDecoration: "none"
        }}>
          <div className="venue-card-container" style={filterImageSytle}>
            <img src={`/${triangle}`} className="venue-card-triangle"/>
            <div className="venue-card-venue-name">
              {name}
            </div>
            <img src={`/${line}`} className="venue-card-line"/>
            <div className="venue-card-bottom-wrapper">
              <div className="venue-card-bottom-line-wrapper">
                <div className="venue-card-here-now">
                  <img src={`/${hereNowCountIcon}`} className="venue-card-here-now-icon"/>
                  {hereNowCount}
                </div>
                <PriceLevelBar tier={tier} className="venue-card-price-level-bar"/>
              </div>
              <div className="venue-card-rate">
                {rating}
              </div>
            </div>
          </div>
        </Link>
      </div>

    );
  }
}

export default VenueCard;
