import React, {Component} from "react";
import PropTypes from "prop-types";

import VenueCard from "../venueCard/VenueCard.jsx";

import "./venueList.scss";

import notFoundPhotoImage from "../../../assets/img/not-found-venue-photo.jpg";

class VenueList extends Component {
  static propTypes = {
    venues: PropTypes.array.isRequired
  };

  render() {
    const {venues} = this.props;

    return (
      <div className="venues-table-container">
        {
          venues.length ?
            venues.map(venueInfo => (<VenueCard id={venueInfo.venue.id}
                                                key={venueInfo.venue.id}
                                                name={venueInfo.venue.name}
                                                hereNowCount={venueInfo.venue.hereNow.count}
                                                priceInfo={venueInfo.venue.price}
                                                rating={venueInfo.venue.rating}
                                                photo={venueInfo.venue.featuredPhotos ?
                                                  `https://igx.4sqi.net/img/general/290x290${venueInfo.venue.featuredPhotos.items[0].suffix}` :
                                                  `/${notFoundPhotoImage}`}/>)) :
            "Loading..."
        }
      </div>
    );
  }
}

export default VenueList;
