import React, {Component} from "react";
import PropTypes from "prop-types";

import VenueInfo from "./venueInfo/VenueInfo.jsx";
import VenuePhotos from "./venuePhotos/venuePhotos/VenuePhotos.jsx";
import VenueTips from "./venueTips/venueTips/VenueTips.jsx";
import Footer from "../components/footer/footer.jsx";

import accessKeys from "../connection/accessKeys.js";

import "./venuePage.scss";

class VenuePage extends Component {
  static prototypes = {
    venueId: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      venueData: "",
      photosData: ""
    };
  }

  componentWillMount() {
    const venueId = this.props.location.pathname.split("/venue/")[1];

    const {clientId, clientSecret} = accessKeys;
    const venueDetailURL = `https://api.foursquare.com/v2/venues/${venueId
      }?&client_id=${clientId
      }&client_secret=${clientSecret
      }&v=20120609`;
    const photoLimit = 12;
    const photosURL = `https://api.foursquare.com/v2/venues/${venueId
      }/photos?limit=${photoLimit
      }&client_id=${clientId
      }&client_secret=${clientSecret
      }&v=20120609`;

    this.fetchData(venueDetailURL).then((json) => {
      this.setState({
        venueData: json.response.venue
      });
    });
    this.fetchData(photosURL).then((json) => {
      this.setState({
        photosData: json.response.photos.items
      });
    });
  }

  fetchData = URL => new Promise((resolve, reject) => {
    fetch(URL).then(res => res.json())
      .then((json) => {
        resolve(json);
      });
  });


  render() {
    const {venueData: {name, rating, location, contact, hereNow, price, tips}, photosData, venueData} = this.state;

    return (
      <div>
        {(photosData && venueData) ?
          (
            <div className="venue-page-container">
              <VenueInfo name={name}
                         rating={rating}
                         address={location.address}
                         phone={contact.formattedPhone}
                         hereNowCount={hereNow.count}
                         priceInfo={price}
                         coverPhoto={venueData.photos.groups[0].items[0]}
                         categoryIcon={`${venueData.categories[0].icon.prefix}64${venueData.categories[0].icon.suffix}`}/>
              <div className="venue-page-inner-container">
                <VenuePhotos photosData={photosData}/>
                <VenueTips tipsData={tips.groups}/>
              </div>
              <div className="venue-page-footer">
                <Footer/>
              </div>
            </div>
          ) :
          (<div>{"Loading..."}</div>)}
      </div>
    );
  }
}

export default VenuePage;
