import React, {Component} from "react";
import PropTypes from "prop-types";

import PriceLevelBar from "../../components/priceLevel/PriceLevelBar.jsx";

import "./venueInfo.scss";

import addressIcon from "../../../assets/img/address-icon.png";
import phoneIcon from "../../../assets/img/phone-icon.png";
import hereNowCountIcon from "../../../assets/img/here-now-icon.png";
import filterImage from "../../../assets/img/filter-image.png";
import headerLogo from "../../../assets/img/venue-page-header-logo.png";
import categoryFrame from "../../../assets/img/category.png";
import ratingImage from "../../../assets/img/rating-rectangle.png";

class VenueInfo extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    rating: PropTypes.number,
    address: PropTypes.string,
    phone: PropTypes.string,
    hereNowCount: PropTypes.number,
    priceInfo: PropTypes.object,
    coverPhoto: PropTypes.object,
    categoryIcon: PropTypes.string
  };

  static defaultProps = {
    rating: 0,
    address: "Address not found",
    phone: "Phone not found",
    hereNowCount: 0
  };

  render() {
    const {
      name, rating, address, phone, hereNowCount, priceInfo,
      coverPhoto: {prefix, suffix}, categoryIcon
    } = this.props;
    const tier = priceInfo ? priceInfo.tier : 0;

    const coverPhotoUrl = `${prefix}1280x600${suffix}`;

    const coverPhotoStyle = {
      backgroundImage: `url(${coverPhotoUrl})`
    };

    const filterImageStyle = {
      backgroundImage: `url(/${filterImage})`
    };

    const categoryFrameStyle = {
      backgroundImage: `url(/${categoryFrame})`
    };

    const ratingBackgroundStyle = {
      backgroundImage: `url(/${ratingImage})`
    };

    return (
      <div className="venue-info-container" style={coverPhotoStyle}>
        <div className="venue-info-container-filter" style={filterImageStyle}>
          <div className="venue-info-header-logos-container">
            <img src={`/${headerLogo}`} className="venue-info-header-logo"/>
            <div style={categoryFrameStyle} className="venue-info-category-frame">
              <img src={categoryIcon} className="venue-info-category-icon"/>
            </div>
          </div>
          <div className="venue-info-venue-name">{name}</div>
          <div className="venue-info-venue-details-container">
            <div className="venue-info-venue-details">
              <div className="venue-info-venue-details-address">
                <img src={`/${addressIcon}`} className="venue-info-venue-details-address-icon"/>
                {address}
              </div>
              <div>
                <img src={`/${phoneIcon}`} className="venue-info-venue-details-phone-icon"/>
                {phone}
              </div>
              <div className="venue-info-venue-details-inner-container">
                <div className="venue-info-venue-details-inner-here-now">
                  <img src={`/${hereNowCountIcon}`} className="venue-info-venue-details-inner-here-now-icon"/>
                  {hereNowCount}
                </div>
                {tier === 0 ?
                  "" :
                  <PriceLevelBar tier={tier}/>}
              </div>
              <div className="venue-info-venue-details-rating" style={ratingBackgroundStyle}>{rating}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VenueInfo;
