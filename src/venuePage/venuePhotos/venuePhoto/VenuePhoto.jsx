import React, {Component} from "react";
import PropTypes from "prop-types";

import notFoundUserPhoto from "../../../../assets/img/not-found-user-photo.png";
import filterImage from "../../../../assets/img/filter-image.png";

import "./venuePhoto.scss";

class VenuePhoto extends Component {
  static PropTypes = {
    photoUrl: PropTypes.string.isRequired,
    userFullName: PropTypes.string,
    userAvatarUrl: PropTypes.string
  };

  static defaultProps = {
    userFullName: "Not found",
    userAvatarUrl: notFoundUserPhoto
  };

  render() {
    const {photoUrl, userFullName, userAvatarUrl} = this.props;


    const filterImageStyle = {
      backgroundImage: `url(/${filterImage})`
    };

    const userImageStyle = {
      backgroundImage: `url(${userAvatarUrl})`
    };

    return (
      <div className="venue-page-photo-container">
        <div>
          <img src={photoUrl}/>
        </div>
        <div className="venue-page-photo-info-container" style={filterImageStyle}>
          <div className="venue-page-photo-info-user-photo-wrapper">
            <div>
              <div className="venue-page-photo-info-user-photo-container">
                <div style={userImageStyle} className="venue-page-photo-info-user-photo"></div>
              </div>
            </div>
          </div>
          <div>
            {userFullName}
          </div>
        </div>
      </div>
    );
  }
}

export default VenuePhoto;
