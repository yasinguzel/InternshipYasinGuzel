import React, {Component} from "react";
import PropTypes from "prop-types";

import VenuePhoto from "../venuePhoto/VenuePhoto.jsx";

import "./venuePhotos.scss";


class VenuePhotos extends Component {
  static prototypes = {
    photosData: PropTypes.object.isRequired
  };

  render() {
    return (
      <div className="venue-page-photos-container">
        {this.props.photosData.map(photoInfo => (
          <VenuePhoto key={photoInfo.suffix}
                      photoUrl={`${photoInfo.prefix}290x290${photoInfo.suffix}`}
                      userFullName={`${photoInfo.user.firstName} ${photoInfo.user.lastName}`}
                      userAvatarUrl={`${photoInfo.user.photo.prefix}60x60${photoInfo.user.photo.suffix}`}/>
        ))}
      </div>
    );
  }
}

export default VenuePhotos;
