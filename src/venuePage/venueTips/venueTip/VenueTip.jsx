import React, {Component} from "react";
import PropTypes from "prop-types";

import notFoundUserPhoto from "../../../../assets/img/not-found-user-photo.png";

import "./venueTip.scss";


class VenueTip extends Component {
  static prototypes = {
    userFullName: PropTypes.string,
    userImgUrl: PropTypes.string,
    text: PropTypes.string.isRequired
  };

  static defaultProps = {
    userFullName: "Not Found",
    userImgUrl: `/${notFoundUserPhoto}`
  };

  render() {
    const {userFullName, userImgUrl, text} = this.props;

    const userImageStyle = {
      backgroundImage: `url(${userImgUrl})`
    };

    return (
      <div className="venue-tip-container">
        <div className="venue-tip-user-image-wrapper">
          <div className="venue-tip-user-image-container">
            <div className="venue-tip-user-image" style={userImageStyle}/>
          </div>
        </div>
        <div className="venue-tip-inner-container">
          <div className="venue-tip-user-full-name">{userFullName}</div>
          <div className="venue-tip-text">{text}</div>
        </div>
      </div>
    );
  }
}

export default VenueTip;
