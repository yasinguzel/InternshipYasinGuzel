import React, {Component} from "react";
import PropTypes from "prop-types";

import VenueTip from "../venueTip/VenueTip.jsx";

import line from "../../../../assets/img/line.png";

import "./venueTips.scss";

import notFoundUserPhoto from "../../../../assets/img/not-found-user-photo.png";

class VenueTips extends Component {
  static propTypes = {
    tipsData: PropTypes.array.isRequired
  };

  render() {
    const {items} = this.props.tipsData[0];

    return (
      <div className="venue-tips-container">
        <div className="venue-tips-header">{"TIPS"}</div>
        <img src={`/${line}`}/>
        <div className="venue-tips-wrapper">
          {items.map((tipsData, index) => (
            <VenueTip key={tipsData.id}
                      text={tipsData.text}
                      userFullName={`${tipsData.user ? tipsData.user.firstName : ""} ${tipsData.user ? tipsData.user.lastName : ""}`}
                      userImgUrl={tipsData.user ? `${tipsData.user.photo.prefix}60x60${tipsData.user.photo.suffix}` : notFoundUserPhoto}/>
          ))}
        </div>
      </div>
    );
  }
}

export default VenueTips;
